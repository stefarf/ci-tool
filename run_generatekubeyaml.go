package main

import (
	"bytes"
	"fmt"
	"os"
	"path"
	"strings"
	"text/template"

	"gitlab.com/stefarf/iferr"
	"gitlab.com/stefarf/vexec"
	"gitlab.com/stefarf/vexec/writerparser/capturewp"
)

type Data map[string]any

func runGenerateKubeYaml(
	app, configPath, env, hash, imageTag, namespace, rootDir, ingressHost, ingressPath string,
	appReplicas, port int,
) {
	configFile := path.Join(rootDir, "config", configPath, fmt.Sprintf("%s.yaml", env))
	configContent := readConfigAndAddIndent(configFile)

	appYamlTmpl := mustReadFile(path.Join(rootDir, "ci/template/app.yaml.tmpl"))
	namespaceYamlTmpl := mustReadFile(path.Join(rootDir, "ci/template/namespace.yaml.tmpl"))

	appYamlFile := path.Join(rootDir, "dist/app.yaml")
	namespaceYamlFile := path.Join(rootDir, "dist/namespace.yaml")
	secretYamlFile := path.Join(rootDir, "dist/secret.yaml")

	appEnv := ""
	secretFileEnv := ""
	switch env {
	case "prod":
		appEnv = app
		secretFileEnv = fmt.Sprintf("%s_%s", namespace, app)
	default:
		appEnv = fmt.Sprintf("%s-%s", app, env)
		secretFileEnv = fmt.Sprintf("%s_%s_%s_SECRET", namespace, app, env)
	}
	secretFileEnv = strings.Replace(secretFileEnv, "-", "_", -1)
	secretFile := mustGetEnv(strings.ToUpper(secretFileEnv))

	ex := vexec.New().ChDir(rootDir).IferrFatal()
	ex.
		SetStdoutParser(capturewp.New()).
		RunCmdArgs(
			"kubectl", "create", "secret", "generic", appEnv, "-n", namespace,
			"--from-file=secret.yaml="+secretFile,
			"--dry-run=client", "-o", "yaml").
		IferrFatal()
	secret := ex.ParseStdoutIfEmptyFatal()
	iferr.Fatal(os.WriteFile(secretYamlFile, []byte(secret), os.ModePerm))

	mustParseExecuteWriteFile(appYamlTmpl, appYamlFile, Data{
		"App":         app,
		"AppEnv":      appEnv,
		"AppPort":     port,
		"AppReplicas": appReplicas,
		"ConfigYaml":  configContent,
		"Hash":        hash,
		"ImageTag":    imageTag,
		"IngressHost": ingressHost,
		"IngressPath": ingressPath,
		"Namespace":   namespace,
	})
	mustParseExecuteWriteFile(namespaceYamlTmpl, namespaceYamlFile, Data{
		"Namespace": namespace,
	})
}

func readConfigAndAddIndent(configFile string) string {
	b, err := os.ReadFile(configFile)
	iferr.Fatal(err)

	var lines []string
	for _, line := range strings.Split(string(b), "\n") {
		lines = append(lines, "    "+line)
	}
	return strings.Join(lines, "\n")
}

func mustParseExecuteWriteFile(text, filename string, data any) {
	s := mustParseExecute(text, data)
	//fmt.Printf(">>> Parse template to: %s <<<\n%s\n>>> end of %s <<<\n\n", filename, s, filename)
	fmt.Println("Parse template to:", filename)
	iferr.Fatal(os.WriteFile(filename, []byte(s), os.ModePerm))
}

func mustParseExecute(text string, data any) string {
	t := template.Must(template.New("").Parse(text))
	var buf bytes.Buffer
	err := t.Execute(&buf, data)
	iferr.Fatal(err)
	return buf.String()
}
