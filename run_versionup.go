package main

import (
	"fmt"
	"log"
	"os"
	"path"
	"regexp"
	"strconv"
	"time"

	"gitlab.com/stefarf/iferr"
	"gitlab.com/stefarf/vexec"
)

const (
	repoCloneDir = "/tmp/repo"
)

func runGitCloneVersionUpAndPush(repoUrl, userEmail, userName, branch, versionFile string) {
	ex := vexec.New()

	// git clone
	ex.
		RunCmdArgs("git", "clone", repoUrl, repoCloneDir).
		ChDir(repoCloneDir).
		Run("git config pull.rebase false").
		RunCmdArgs("git", "config", "--global", "user.email", userEmail).
		RunCmdArgs("git", "config", "--global", "user.name", userName).
		Run("git fetch --all").
		RunCmdArgs("git", "reset", "--hard", "origin/"+branch).
		IferrFatal()

	newVersion := versionUp(path.Join(repoCloneDir, versionFile))

	// git add and push
	ex.
		ChDir(repoCloneDir).
		Run("git add --all").
		RunCmdArgs("git", "commit", "-m", "Version: "+newVersion).
		RunCmdArgs("git", "push", "origin", "HEAD:"+branch).
		IferrFatal()
}

func versionUp(versionFile string) string {
	version := readVersion(versionFile)
	newVersion := parseVersionAndIncrement(version)
	iferr.Fatal(os.WriteFile(versionFile, []byte(newVersion), os.ModePerm))
	return newVersion
}

func readVersion(versionFile string) string {
	b, err := os.ReadFile(versionFile)
	iferr.Fatal(err)
	return string(b)
}

func parseVersionAndIncrement(ver string) string {
	re := regexp.MustCompile(`(\d+)(\.(\d+))?(\.(\d+))?`)
	rst := re.FindAllStringSubmatch(ver, -1)

	if len(rst) != 1 {
		log.Fatal("unexpected VERSION:", ver)
	}
	major := rst[0][1]
	year := rst[0][3]
	count := rst[0][5]

	majorVer, err := strconv.ParseInt(major, 10, 32)
	iferr.Fatal(err)

	var yearVer, countVer int64
	yearNow := int64(time.Now().Year())

	if year != "" {
		yearVer, err = strconv.ParseInt(year, 10, 32)
		iferr.Fatal(err)
	}
	if yearVer != yearNow {
		yearVer = yearNow
	} else if count != "" {
		countVer, err = strconv.ParseInt(count, 10, 32)
		iferr.Fatal(err)
		countVer++
	}

	return fmt.Sprintf("%d.%d.%d", majorVer, yearVer, countVer)
}
