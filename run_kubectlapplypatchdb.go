package main

import (
	"encoding/json"
	"fmt"
	"path"

	"gitlab.com/stefarf/iferr"
	"gitlab.com/stefarf/vexec"
	"gitlab.com/stefarf/vexec/writerparser/capturewp"
)

const defaultDBRootPassword = "secret"

func runKubectlApplyPatchDB(app, env, namespace, rootDir, storageSize string) {
	dbYamlTmpl := mustReadFile(path.Join(rootDir, "ci/template/db.yaml.tmpl"))

	appEnv := ""
	switch env {
	case "prod":
		appEnv = app
	default:
		appEnv = fmt.Sprintf("%s-%s", app, env)
	}

	ex := vexec.New().ChDir(rootDir).IferrFatal()
	existingPvc := ex.
		SetStdoutParser(capturewp.New()).
		RunCmdArgs("kubectl", "-n", namespace,
			"get", "pvc", fmt.Sprintf("%s-db-%s-db-0", appEnv, appEnv),
			"-o", "json").
		ClearError().
		ParseStdout()

	if existingPvc == "" {
		ex.
			SetStringAsStdin(mustParseExecute(dbYamlTmpl, Data{
				"AppEnv":       appEnv,
				"Namespace":    namespace,
				"DBRootPasswd": defaultDBRootPassword,
				"StorageSize":  storageSize,
			})).
			RunCmdArgs("kubectl", "apply", "-f", "-").
			IferrFatal()
		return
	}

	var pvc struct {
		Status struct {
			Capacity struct {
				Storage string
			}
		}
	}
	iferr.Fatal(json.Unmarshal([]byte(existingPvc), &pvc))

	if pvc.Status.Capacity.Storage == storageSize {
		return
	}

	var req struct {
		Spec struct {
			Resources struct {
				Requests struct {
					Storage string `json:"storage"`
				} `json:"requests"`
			} `json:"resources"`
		} `json:"spec"`
	}
	req.Spec.Resources.Requests.Storage = storageSize
	b, err := json.Marshal(req)
	iferr.Panic(err)
	ex.
		RunCmdArgs("kubectl", "-n", namespace, "patch",
			"pvc", fmt.Sprintf("%s-db-%s-db-0", appEnv, appEnv),
			"-p", string(b)).
		IferrFatal()
}
