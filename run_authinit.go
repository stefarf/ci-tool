package main

import "gitlab.com/stefarf/vexec"

func runAuthInit(loginRegistry, saveKubeconfig bool) {
	ex := vexec.New()
	ex.Run("doctl auth init --access-token " + mustGetEnv("DO_TOKEN")).IferrFatal()
	if loginRegistry {
		ex.Run("doctl registry login").IferrFatal()
	}
	if saveKubeconfig {
		ex.Run("doctl kubernetes cluster kubeconfig save vostra").IferrFatal()
	}
}
