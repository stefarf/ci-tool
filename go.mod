module gitlab.com/stefarf/ci-tool

go 1.19

require gitlab.com/stefarf/iferr v0.1.1

require gitlab.com/stefarf/vexec v0.1.2 // indirect
