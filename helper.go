package main

import (
	"log"
	"os"

	"gitlab.com/stefarf/iferr"
)

func mustGetEnv(key string) string {
	val := os.Getenv(key)
	if val == "" {
		log.Fatalf("unknown environment: %s", key)
	}
	return val
}

func mustReadFile(filename string) string {
	b, err := os.ReadFile(filename)
	iferr.Fatal(err)
	return string(b)
}
