package main

import (
	"flag"
	"log"
)

var (
	fRun = flag.String("run", "", "command to run")

	fApp             = flag.String("app", "", "app name")
	fAppPort         = flag.Int("appPort", 8000, "app port")
	fAppReplicas     = flag.Int("appReplicas", 0, "number of app replicas / instances")
	fBranch          = flag.String("branch", "", "git branch")
	fConfigPath      = flag.String("configPath", "server", "the path in the config directory to find the configuration")
	fEnv             = flag.String("env", "", "environment: 'prod' or else")
	fHash            = flag.String("hash", "", "hash/sha256 from 'docker image --digests'")
	fImageTag        = flag.String("imageTag", "", "{IMAGE}:{TAG}")
	fIngressHost     = flag.String("ingressHost", "", "ingress host")
	fIngressPath     = flag.String("ingressPath", "", "ingress path")
	fLoginRegistry   = flag.Bool("loginRegistry", false, "")
	fMaintainerEmail = flag.String("maintainerEmail", "arief@vostra.co.id", "maintainer email")
	fMaintainerUser  = flag.String("maintainerUser", "stefarf", "maintainer user name")
	fNamespace       = flag.String("namespace", "", "namespace / client namespace")
	fRepoUrl         = flag.String("repoUrl", "", "git repository url")
	fRootDir         = flag.String("rootDir", "", "git root directory of this project")
	fSaveKubeconfig  = flag.Bool("saveKubeconfig", false, "")
	fStorageSize     = flag.String("storageSize", "", "db filesystem storage size")
	fVersionFile     = flag.String("versionFile", "", "a file name containing the app version")
)

func main() {
	flag.Parse()
	switch *fRun {

	case "auth-init":
		runAuthInit(*fLoginRegistry, *fSaveKubeconfig)

	case "get-hash":
		if *fImageTag == "" {
			log.Fatal("empty -imageTag")
		}
		runGetHash(*fImageTag)

	case "generate-kube-yaml":
		fatalIfEmptyString(*fApp, "-app")
		fatalIfEmptyInt(*fAppReplicas, "-appReplicas")
		fatalIfEmptyString(*fConfigPath, "-configPath")
		fatalIfEmptyString(*fEnv, "-env")
		fatalIfEmptyString(*fHash, "-hash")
		fatalIfEmptyString(*fImageTag, "-imageTag")
		fatalIfEmptyString(*fIngressHost, "-ingressHost")
		fatalIfEmptyString(*fIngressPath, "-ingressPath")
		fatalIfEmptyString(*fNamespace, "-namespace")
		fatalIfEmptyString(*fRootDir, "-rootDir")

		runGenerateKubeYaml(
			*fApp, *fConfigPath, *fEnv, *fHash, *fImageTag, *fNamespace, *fRootDir,
			*fIngressHost, *fIngressPath,
			*fAppReplicas, *fAppPort)

	case "git-clone-version-up-and-push":
		fatalIfEmptyString(*fBranch, "-branch")
		fatalIfEmptyString(*fMaintainerEmail, "-maintainerEmail")
		fatalIfEmptyString(*fMaintainerUser, "-maintainerUser")
		fatalIfEmptyString(*fRepoUrl, "-repoUrl")
		fatalIfEmptyString(*fVersionFile, "-versionFile")

		runGitCloneVersionUpAndPush(*fRepoUrl, *fMaintainerEmail, *fMaintainerUser, *fBranch, *fVersionFile)

	case "kubectl-apply-patch-db":
		fatalIfEmptyString(*fApp, "-app")
		fatalIfEmptyString(*fEnv, "-env")
		fatalIfEmptyString(*fNamespace, "-namespace")
		fatalIfEmptyString(*fRootDir, "-rootDir")
		fatalIfEmptyString(*fStorageSize, "-storageSize")

		runKubectlApplyPatchDB(*fApp, *fEnv, *fNamespace, *fRootDir, *fStorageSize)

	case "":
		log.Fatal("empty -run")
	default:
		log.Fatalf("invalid -run '%s'", *fRun)
	}
}

func fatalIfEmptyString(s, name string) {
	if s == "" {
		log.Fatalf("empty %s", name)
	}
}

func fatalIfEmptyInt(i int, name string) {
	if i == 0 {
		log.Fatalf("empty %s", name)
	}
}
