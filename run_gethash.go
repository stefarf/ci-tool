package main

import (
	"fmt"
	"io"
	"log"
	"os"
	"strings"

	"gitlab.com/stefarf/iferr"
)

func runGetHash(imageTag string) {
	s := strings.Split(imageTag, ":")
	if len(s) != 2 {
		log.Fatal("invalid -imageTag")
	}
	image, tag := s[0], s[1]
	hash := readStdinAndGetHash(image, tag)
	fmt.Println(hash)
}

func readStdinAndGetHash(image, tag string) string {
	b, err := io.ReadAll(os.Stdin)
	iferr.Fatal(err)

	for _, s := range strings.Split(string(b), "\n") {
		var words []string
		for _, word := range strings.Split(s, " ") {
			if word != "" {
				words = append(words, word)
			}
		}
		if len(words) >= 3 && words[0] == image && words[1] == tag {
			return words[2]
		}
	}
	return ""
}
